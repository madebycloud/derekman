// @codekit-prepend 'vendor/jquery.breakpoint.js'

(function($)
{
    var $window   = $(window);
    var $document = $(document);

    $(function()
    {
        $('.slideshow').each(function()
        {
            var slideshowIsEnabled = false;

            // Get all relevant jQuery objects
            var $slideshow  = $(this),
                $slides     = $slideshow.children('.slideshow-slides'),
                $slide      = $slides.children(),
                $slideImage = $slide.find('.slide-image'),
                $controls   = $slideshow.children('.slideshow-controls');

            // Get image area
            var widestImageWidth = 0,
                shortestImageHeight = 10000;

            $slide.find('.slide-image').each(function()
            {
                var self = $(this);
                widestImageWidth = Math.max(self.data('width'), widestImageWidth);
                shortestImageHeight = Math.min(self.data('height'), shortestImageHeight);
            });

            // Left and right keys for cycle slideshow
            var handleSlideshowKeys = function(event)
            {
                if (event.keyCode == 37)
                {
                    $slides.cycle('prev');
                }
                else if (event.keyCode == 39)
                {
                    $slides.cycle('next');
                }
            };

            // Adjust slideshow sizes when window is resized
            var handleSlideshowResize = function()
            {
                if (slideshowIsEnabled)
                {
                    var slidesHeight = $window.height() - $controls.outerHeight(true);
                    $slides.height(slidesHeight);

                    var tallestCaptionHeight = 0;
                    $slide.find('.slide-caption').each(function()
                    {
                        tallestCaptionHeight = Math.max(tallestCaptionHeight, $(this).outerHeight(true));
                    });

                    $slideImage.css(
                    {
                        height: $slide.width() * (shortestImageHeight / widestImageWidth),
                        maxHeight: Math.min($slide.height() - tallestCaptionHeight, shortestImageHeight)
                    });
                }
            };

            // Initialise the slideshow
            var enableSlideshow = function()
            {
                slideshowIsEnabled = true;

                $controls.toggle($slide.length > 1);

                $slides.cycle(
                {
                    paused: true,
                    slides: $slide,
                    manualTrump: false,
                    next: $controls.find('.slideshow-next').add($slideImage),
                    prev: $controls.find('.slideshow-prev'),
                    caption: $controls.find('.slideshow-caption'),
                    captionTemplate: '{{slideNum}} of {{slideCount}}'
                });

                if ($slide.length > 1)
                {
                    $document.keyup(handleSlideshowKeys);
                }

                $window.resize(handleSlideshowResize);
                handleSlideshowResize();
                setTimeout(handleSlideshowResize, 500);
            };

            // Destory the slideshow
            var disableSlideshow = function()
            {
                slideshowIsEnabled = false;

                $document.off('keyup', handleSlideshowKeys);
                $window.off('resize', handleSlideshowResize);

                $slides.cycle('destroy').height('');

                $slideImage.css(
                {
                    height: '',
                    maxHeight: ''
                });
            };

            $.breakpoint(
            {
                condition: function ()
                {
                    return window.matchMedia('only screen and (min-width: 769px)').matches;
                },
                enter: function()
                {
                    enableSlideshow();
                },
                exit: function()
                {
                    disableSlideshow();
                }
            });
        });
    });
}(window.jQuery));
