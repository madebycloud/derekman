<?php

if (isset($_SERVER['HTTPS']) && (strcasecmp($_SERVER['HTTPS'],'on') === 0 || $_SERVER['HTTPS'] == 1))
{
	$protocol = 'https://';
}
else if ( ! empty($_SERVER['HTTP_X_FORWARDED_PROTO']))
{
	$protocol = $_SERVER['HTTP_X_FORWARDED_PROTO'] . '://';
}
else if (isset($_SERVER['HTTP_CF_VISITOR']))
{
	$cloudFlareVisitor = json_decode($_SERVER['HTTP_CF_VISITOR']);

	if ($cloudFlareVisitor->scheme == 'https')
	{
		$protocol = 'https://';
	}
}
else
{
	$protocol = 'http://';
}

$siteUrl = $protocol . $_SERVER['HTTP_HOST'];
$fileSystemPath = realpath('..');

return array(

	'*' => array(
		'omitScriptNameInUrls' => true,
		'siteUrl'              => $siteUrl,
		'baseCpUrl'            => $siteUrl,

		'environmentVariables' => array(
			'siteUrl'          => $siteUrl,
			'fileSystemPath'   => $fileSystemPath,
		),
	),

	'.dev' => array(
		'devMode' => true,
	),

);
